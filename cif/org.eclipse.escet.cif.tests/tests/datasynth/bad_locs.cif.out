Reading CIF file "datasynth/bad_locs.cif".
Preprocessing CIF specification.
Converting CIF specification to internal format.

CIF variables and location pointers:
  Nr     Kind               Type       Name  Group  BDD vars  CIF values  BDD values  Values used
  -----  -----------------  ---------  ----  -----  --------  ----------  ----------  -----------
  1      location pointer   n/a        p     0      3 * 2     7 * 2       8 * 2       ~88%
  2      discrete variable  int[0..9]  p.x   1      4 * 2     10 * 2      16 * 2      ~63%
  -----  -----------------  ---------  ----  -----  --------  ----------  ----------  -----------
  Total                                      2      14        34          48          ~71%

Applying automatic variable ordering:
  Number of hyperedges: 9

  Applying FORCE algorithm:
    Maximum number of iterations: 10

    Total span:                    2 (total)                 0.22 (avg/edge) [before]
    Total span:                    2 (total)                 0.22 (avg/edge) [iteration 1]
    Total span:                    2 (total)                 0.22 (avg/edge) [after]

  Applying sliding window algorithm:
    Window length: 2

    Total span:                    2 (total)                 0.22 (avg/edge) [before]
    Total span:                    2 (total)                 0.22 (avg/edge) [after]

  Variable order unchanged.

Starting data-based synthesis.

Invariant (components state plant inv):      true
Invariant (locations state plant invariant): true
Invariant (system state plant invariant):    true

Invariant (components state req invariant):  true
Invariant (locations state req invariant):   true
Invariant (system state req invariant):      true

Initial   (discrete variable 1):             p.x = 0
Initial   (discrete variables):              p.x = 0
Initial   (components init predicate):       true
Initial   (aut/locs init predicate):         p.l0
Initial   (auts/locs init predicate):        p.l0
Initial   (uncontrolled system):             p.l0 and p.x = 0
Initial   (system, combined init/plant inv): p.l0 and p.x = 0
Initial   (system, combined init/state inv): p.l0 and p.x = 0

Marked    (components marker predicate):     true
Marked    (aut/locs marker predicate):       not p.l4 and (not p.l5 and not p.l6)
Marked    (auts/locs marker predicate):      not p.l4 and (not p.l5 and not p.l6)
Marked    (uncontrolled system):             not p.l4 and (not p.l5 and not p.l6)
Marked    (system, combined mark/plant inv): not p.l4 and (not p.l5 and not p.l6)
Marked    (system, combined mark/state inv): not p.l4 and (not p.l5 and not p.l6)

State/event exclusion plants:
  None

State/event exclusion requirements:
  Event "u" needs:
    p.x != 5

Uncontrolled system:
  State: (controlled-behavior: ?)
    Edge: (event: c) (guard: p.l0) (assignments: p.x := 1, p := p.l1)
    Edge: (event: c) (guard: p.l2) (assignments: p.x := 3, p := p.l3)
    Edge: (event: c) (guard: p.l4) (assignments: p.x := 5, p := p.l5)
    Edge: (event: u) (guard: p.l1) (assignments: p.x := 2, p := p.l2)
    Edge: (event: u) (guard: p.l3) (assignments: p.x := 4, p := p.l4)
    Edge: (event: u) (guard: p.l5) (assignments: p.x := 6, p := p.l6)

Restricting behavior using state/event exclusion plants.

Initialized controlled-behavior predicate using invariants: true.

Extending controlled-behavior predicate using variable ranges.

Controlled behavior: true -> true [range: true, variable: location pointer for automaton "p" (group: 0, domain: 0+1, BDD variables: 3, CIF/BDD values: 7/8)].
Controlled behavior: true -> true [range: true, variable: discrete variable "p.x" of type "int[0..9]" (group: 1, domain: 2+3, BDD variables: 4, CIF/BDD values: 10/16)].

Extended controlled-behavior predicate using variable ranges: true.

Restricting behavior using state/event exclusion requirements.

Controlled behavior: true -> not p.l1 or p.x != 5 [requirement: p.x != 5, edge: (event: u) (guard: p.l1) (assignments: p.x := 2, p := p.l2)].
Controlled behavior: not p.l1 or p.x != 5 -> (not p.l1 or p.x != 5) and (not p.l3 or p.x != 5) [requirement: p.x != 5, edge: (event: u) (guard: p.l3) (assignments: p.x := 4, p := p.l4)].
Controlled behavior: (not p.l1 or p.x != 5) and (not p.l3 or p.x != 5) -> (not p.l1 and not p.l5 or p.x != 5) and (not p.l3 or p.x != 5) [requirement: p.x != 5, edge: (event: u) (guard: p.l5) (assignments: p.x := 6, p := p.l6)].

Restricted behavior using state/event exclusion requirements:
  State: (controlled-behavior: (not p.l1 and not p.l5 or p.x != 5) and (not p.l3 or p.x != 5))

Round 1: started.

Round 1: computing backward controlled-behavior predicate.
Backward controlled-behavior: not p.l4 and (not p.l5 and not p.l6) [marker predicate]
Backward controlled-behavior: not p.l4 and (not p.l5 and not p.l6) -> not p.l4 and not p.l6 and ((not p.l1 and not p.l3 or p.x != 5) and not p.l5) [restricted to current/previous controlled-behavior predicate: (not p.l1 and not p.l5 or p.x != 5) and (not p.l3 or p.x != 5)]
Backward reachability: iteration 1.
Backward controlled-behavior: not p.l4 and not p.l6 and ((not p.l1 and not p.l3 or p.x != 5) and not p.l5) [fixed point].
Controlled behavior: (not p.l1 and not p.l5 or p.x != 5) and (not p.l3 or p.x != 5) -> not p.l4 and not p.l6 and ((not p.l1 and not p.l3 or p.x != 5) and not p.l5).

Round 1: computing backward uncontrolled bad-state predicate.
Backward uncontrolled bad-state: p.l4 or p.l6 or ((p.l1 or p.l3) and p.x = 5 or p.l5) [current/previous controlled behavior predicate]
Backward reachability: iteration 1.
Backward uncontrolled bad-state: p.l4 or p.l6 or ((p.l1 or p.l3) and p.x = 5 or p.l5) -> p.l4 or p.l6 or (p.l1 and p.x = 5 or (p.l5 or p.l3)) [backward reach with edge: (event: u) (guard: p.l3) (assignments: p.x := 4, p := p.l4)]
Backward reachability: iteration 2.
Backward uncontrolled bad-state: p.l4 or p.l6 or (p.l1 and p.x = 5 or (p.l5 or p.l3)) [fixed point].
Controlled behavior: not p.l4 and not p.l6 and ((not p.l1 and not p.l3 or p.x != 5) and not p.l5) -> not p.l4 and not p.l6 and ((not p.l1 or p.x != 5) and (not p.l5 and not p.l3)).

Round 1: computing forward controlled-behavior predicate.
Forward controlled-behavior: p.l0 and p.x = 0 [initialization predicate]
Forward reachability: iteration 1.
Forward controlled-behavior: p.l0 and p.x = 0 -> p.l0 and p.x = 0 or p.l1 and p.x = 1 [forward reach with edge: (event: c) (guard: p.l0) (assignments: p.x := 1, p := p.l1), restricted to current/previous controlled-behavior predicate: not p.l4 and not p.l6 and ((not p.l1 or p.x != 5) and (not p.l5 and not p.l3))]
Forward controlled-behavior: p.l0 and p.x = 0 or p.l1 and p.x = 1 -> p.l0 and p.x = 0 or (p.l2 and p.x = 2 or p.l1 and p.x = 1) [forward reach with edge: (event: u) (guard: p.l1) (assignments: p.x := 2, p := p.l2), restricted to current/previous controlled-behavior predicate: not p.l4 and not p.l6 and ((not p.l1 or p.x != 5) and (not p.l5 and not p.l3))]
Forward reachability: iteration 2.
Forward controlled-behavior: p.l0 and p.x = 0 or (p.l2 and p.x = 2 or p.l1 and p.x = 1) [fixed point].
Controlled behavior: not p.l4 and not p.l6 and ((not p.l1 or p.x != 5) and (not p.l5 and not p.l3)) -> p.l0 and p.x = 0 or (p.l2 and p.x = 2 or p.l1 and p.x = 1).

Round 1: finished, need another round.

Round 2: started.

Round 2: computing backward controlled-behavior predicate.
Backward controlled-behavior: not p.l4 and (not p.l5 and not p.l6) [marker predicate]
Backward controlled-behavior: not p.l4 and (not p.l5 and not p.l6) -> p.l0 and p.x = 0 or (p.l2 and p.x = 2 or p.l1 and p.x = 1) [restricted to current/previous controlled-behavior predicate: p.l0 and p.x = 0 or (p.l2 and p.x = 2 or p.l1 and p.x = 1)]
Backward reachability: iteration 1.
Backward controlled-behavior: p.l0 and p.x = 0 or (p.l2 and p.x = 2 or p.l1 and p.x = 1) [fixed point].

Round 2: computing backward uncontrolled bad-state predicate.
Backward uncontrolled bad-state: (not p.l0 or p.x != 0) and ((not p.l2 or p.x != 2) and (not p.l1 or p.x != 1)) [current/previous controlled behavior predicate]
Backward reachability: iteration 1.

Round 2: finished, controlled behavior is stable.

Computing controlled system guards.

Edge (event: c) (guard: p.l2) (assignments: p.x := 3, p := p.l3): guard: p.l2 -> false.
Edge (event: c) (guard: p.l4) (assignments: p.x := 5, p := p.l5): guard: p.l4 -> false.

Final synthesis result:
  State: (controlled-behavior: p.l0 and p.x = 0 or (p.l2 and p.x = 2 or p.l1 and p.x = 1))
    Edge: (event: c) (guard: p.l0) (assignments: p.x := 1, p := p.l1)
    Edge: (event: c) (guard: p.l2 -> false) (assignments: p.x := 3, p := p.l3)
    Edge: (event: c) (guard: p.l4 -> false) (assignments: p.x := 5, p := p.l5)
    Edge: (event: u) (guard: p.l1) (assignments: p.x := 2, p := p.l2)
    Edge: (event: u) (guard: p.l3) (assignments: p.x := 4, p := p.l4)
    Edge: (event: u) (guard: p.l5) (assignments: p.x := 6, p := p.l6)

Controlled system:                     exactly 3 states.

Initial (synthesis result):            p.l0 and p.x = 0 or (p.l2 and p.x = 2 or p.l1 and p.x = 1)
Initial (uncontrolled system):         p.l0 and p.x = 0
Initial (controlled system):           p.l0 and p.x = 0
Initial (removed by supervisor):       false
Initial (added by supervisor):         true

Simplification of controlled system under the assumption of the plants:
  Event c: guard: p.l0 -> p.l0 or p.l1 [assume p.l0 or (p.l4 or p.l2)].

Constructing output CIF specification.
Writing output CIF file "datasynth/bad_locs.ctrlsys.real.cif".
