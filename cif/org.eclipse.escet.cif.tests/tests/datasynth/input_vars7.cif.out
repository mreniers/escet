Reading CIF file "datasynth/input_vars7.cif".
Preprocessing CIF specification.
Converting CIF specification to internal format.

CIF variables and location pointers:
  Nr     Kind               Type  Name       Group  BDD vars  CIF values  BDD values  Values used
  -----  -----------------  ----  ---------  -----  --------  ----------  ----------  -----------
  1      input variable     bool  io.sensor  0      1 * 2     2 * 2       2 * 2       100%
  2      location pointer   n/a   p          1      1 * 2     2 * 2       2 * 2       100%
  3      discrete variable  bool  p.b        2      1 * 2     2 * 2       2 * 2       100%
  -----  -----------------  ----  ---------  -----  --------  ----------  ----------  -----------
  Total                                      3      6         12          12          100%

Applying automatic variable ordering:
  Number of hyperedges: 1

  Applying FORCE algorithm:
    Maximum number of iterations: 20

    Total span:                    1 (total)                 1.00 (avg/edge) [before]
    Total span:                    1 (total)                 1.00 (avg/edge) [iteration 1]
    Total span:                    1 (total)                 1.00 (avg/edge) [after]

  Applying sliding window algorithm:
    Window length: 3

    Total span:                    1 (total)                 1.00 (avg/edge) [before]
    Total span:                    1 (total)                 1.00 (avg/edge) [after]

  Variable order unchanged.

Starting data-based synthesis.

Invariant (components state plant inv):      true
Invariant (locations state plant invariant): true
Invariant (system state plant invariant):    true

Invariant (components state req invariant):  true
Invariant (locations state req invariant):   true
Invariant (system state req invariant):      true

Initial   (discrete variable 2):             (io.sensor or not p.b) and (not io.sensor or p.b)
Initial   (discrete variables):              (io.sensor or not p.b) and (not io.sensor or p.b)
Initial   (components init predicate):       true
Initial   (aut/locs init predicate):         p.test
Initial   (auts/locs init predicate):        p.test
Initial   (uncontrolled system):             not io.sensor and (p.test and not p.b) or io.sensor and (p.test and p.b)
Initial   (system, combined init/plant inv): not io.sensor and (p.test and not p.b) or io.sensor and (p.test and p.b)
Initial   (system, combined init/state inv): not io.sensor and (p.test and not p.b) or io.sensor and (p.test and p.b)

Marked    (components marker predicate):     true
Marked    (aut/locs marker predicate):       p.done
Marked    (auts/locs marker predicate):      p.done
Marked    (uncontrolled system):             p.done
Marked    (system, combined mark/plant inv): p.done
Marked    (system, combined mark/state inv): p.done

State/event exclusion plants:
  None

State/event exclusion requirements:
  None

Uncontrolled system:
  State: (controlled-behavior: ?)
    Edge: (event: p.u_ok) (guard: p.test and p.b) (assignments: p := p.done)
    Edge: (event: io.sensor) (guard: true) (assignments: io.sensor+ != io.sensor)

Restricting behavior using state/event exclusion plants.

Initialized controlled-behavior predicate using invariants: true.

Extending controlled-behavior predicate using variable ranges.

Restricting behavior using state/event exclusion requirements.

Round 1: started.

Round 1: computing backward controlled-behavior predicate.
Backward controlled-behavior: p.done [marker predicate]
Backward reachability: iteration 1.
Backward controlled-behavior: p.done -> p.done or p.b [backward reach with edge: (event: p.u_ok) (guard: p.test and p.b) (assignments: p := p.done), restricted to current/previous controlled-behavior predicate: true]
Backward reachability: iteration 2.
Backward controlled-behavior: p.done or p.b [fixed point].
Controlled behavior: true -> p.done or p.b.

Round 1: computing backward uncontrolled bad-state predicate.
Backward uncontrolled bad-state: p.test and not p.b [current/previous controlled behavior predicate]
Backward reachability: iteration 1.

Round 1: computing forward controlled-behavior predicate.
Forward controlled-behavior: not io.sensor and (p.test and not p.b) or io.sensor and (p.test and p.b) [initialization predicate]
Forward controlled-behavior: not io.sensor and (p.test and not p.b) or io.sensor and (p.test and p.b) -> io.sensor and (p.test and p.b) [restricted to current/previous controlled-behavior predicate: p.done or p.b]
Forward reachability: iteration 1.
Forward controlled-behavior: io.sensor and (p.test and p.b) -> io.sensor and p.b [forward reach with edge: (event: p.u_ok) (guard: p.test and p.b) (assignments: p := p.done), restricted to current/previous controlled-behavior predicate: p.done or p.b]
Forward controlled-behavior: io.sensor and p.b -> p.b [forward reach with edge: (event: io.sensor) (guard: true) (assignments: io.sensor+ != io.sensor), restricted to current/previous controlled-behavior predicate: p.done or p.b]
Forward reachability: iteration 2.
Forward controlled-behavior: p.b [fixed point].
Controlled behavior: p.done or p.b -> p.b.

Round 1: finished, need another round.

Round 2: started.

Round 2: computing backward controlled-behavior predicate.
Backward controlled-behavior: p.done [marker predicate]
Backward controlled-behavior: p.done -> p.done and p.b [restricted to current/previous controlled-behavior predicate: p.b]
Backward reachability: iteration 1.
Backward controlled-behavior: p.done and p.b -> p.b [backward reach with edge: (event: p.u_ok) (guard: p.test and p.b) (assignments: p := p.done), restricted to current/previous controlled-behavior predicate: p.b]
Backward reachability: iteration 2.
Backward controlled-behavior: p.b [fixed point].

Round 2: computing backward uncontrolled bad-state predicate.
Backward uncontrolled bad-state: not p.b [current/previous controlled behavior predicate]
Backward reachability: iteration 1.

Round 2: finished, controlled behavior is stable.

Computing controlled system guards.

Final synthesis result:
  State: (controlled-behavior: p.b)
    Edge: (event: p.u_ok) (guard: p.test and p.b) (assignments: p := p.done)
    Edge: (event: io.sensor) (guard: true) (assignments: io.sensor+ != io.sensor)

Controlled system:                     exactly 4 states.

Initial (synthesis result):            p.b
Initial (uncontrolled system):         not io.sensor and (p.test and not p.b) or io.sensor and (p.test and p.b)
Initial (controlled system):           io.sensor and (p.test and p.b)
Initial (removed by supervisor):       not io.sensor and (p.test and not p.b)
Initial (added by supervisor):         io.sensor or (p.done or p.b)

Simplification of controlled system initialization predicate under the assumption of the uncontrolled system initialization predicates:
  Initial: io.sensor and (p.test and p.b) -> io.sensor [assume not io.sensor and (p.test and not p.b) or io.sensor and (p.test and p.b)].

Constructing output CIF specification.
Writing output CIF file "datasynth/input_vars7.ctrlsys.real.cif".
