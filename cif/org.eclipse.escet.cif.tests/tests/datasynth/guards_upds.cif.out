Reading CIF file "datasynth/guards_upds.cif".
Preprocessing CIF specification.
Converting CIF specification to internal format.

CIF variables and location pointers:
  Nr     Kind               Type       Name  Group  BDD vars  CIF values  BDD values  Values used
  -----  -----------------  ---------  ----  -----  --------  ----------  ----------  -----------
  1      location pointer   n/a        p1    0      1 * 2     2 * 2       2 * 2       100%
  2      discrete variable  int[0..5]  p1.x  1      3 * 2     6 * 2       8 * 2       75%
  3      discrete variable  int[0..5]  p2.y  2      3 * 2     6 * 2       8 * 2       75%
  -----  -----------------  ---------  ----  -----  --------  ----------  ----------  -----------
  Total                                      3      14        28          36          ~78%

Applying automatic variable ordering:
  Number of hyperedges: 7

  Applying FORCE algorithm:
    Maximum number of iterations: 20

    Total span:                    3 (total)                 0.43 (avg/edge) [before]
    Total span:                    3 (total)                 0.43 (avg/edge) [iteration 1]
    Total span:                    3 (total)                 0.43 (avg/edge) [after]

  Applying sliding window algorithm:
    Window length: 3

    Total span:                    3 (total)                 0.43 (avg/edge) [before]
    Total span:                    3 (total)                 0.43 (avg/edge) [after]

  Variable order unchanged.

Starting data-based synthesis.

Invariant (components state plant inv):      true
Invariant (locations state plant invariant): true
Invariant (system state plant invariant):    true

Invariant (components state req invariant):  true
Invariant (locations state req invariant):   true
Invariant (system state req invariant):      true

Initial   (discrete variable 1):             p1.x = 0
Initial   (discrete variable 2):             p2.y = 0
Initial   (discrete variables):              p1.x = 0 and p2.y = 0
Initial   (components init predicate):       true
Initial   (aut/locs init predicate):         p1.l0
Initial   (aut/locs init predicate):         true
Initial   (auts/locs init predicate):        p1.l0
Initial   (uncontrolled system):             p1.l0 and (p1.x = 0 and p2.y = 0)
Initial   (system, combined init/plant inv): p1.l0 and (p1.x = 0 and p2.y = 0)
Initial   (system, combined init/state inv): p1.l0 and (p1.x = 0 and p2.y = 0)

Marked    (components marker predicate):     true
Marked    (aut/locs marker predicate):       p1.l0
Marked    (aut/locs marker predicate):       true
Marked    (auts/locs marker predicate):      p1.l0
Marked    (uncontrolled system):             p1.l0
Marked    (system, combined mark/plant inv): p1.l0
Marked    (system, combined mark/state inv): p1.l0

State/event exclusion plants:
  None

State/event exclusion requirements:
  None

Uncontrolled system:
  State: (controlled-behavior: ?)
    Edge: (event: a) (guard: p1.l0 and p2.y = 4 or (p1.l0 and p2.y = 5 or p1.l0 and p2.y = 3)) (assignments: p1.x := p1.x + 1, p1 := p1.l1, p2.y := 3)
    Edge: (event: b) (guard: p1.l0 and p1.x = 5 or p1.l0 and p1.x = 3)

Restricting behavior using state/event exclusion plants.

Initialized controlled-behavior predicate using invariants: true.

Extending controlled-behavior predicate using variable ranges.

Controlled behavior: true -> true [range: true, variable: discrete variable "p1.x" of type "int[0..5]" (group: 1, domain: 2+3, BDD variables: 3, CIF/BDD values: 6/8)].
Controlled behavior: true -> true [range: true, variable: discrete variable "p2.y" of type "int[0..5]" (group: 2, domain: 4+5, BDD variables: 3, CIF/BDD values: 6/8)].

Extended controlled-behavior predicate using variable ranges: true.

Restricting behavior using state/event exclusion requirements.

Round 1: started.

Round 1: computing backward controlled-behavior predicate.
Backward controlled-behavior: p1.l0 [marker predicate]
Backward controlled-behavior: p1.l0 -> p1.l0 [restricted to current/previous controlled-behavior predicate: true]
Backward reachability: iteration 1.
Backward controlled-behavior: p1.l0 [fixed point].
Controlled behavior: true -> p1.l0.

Round 1: computing backward uncontrolled bad-state predicate.
Backward uncontrolled bad-state: p1.l1 [current/previous controlled behavior predicate]
Backward reachability: iteration 1.

Round 1: computing forward controlled-behavior predicate.
Forward controlled-behavior: p1.l0 and (p1.x = 0 and p2.y = 0) [initialization predicate]
Forward reachability: iteration 1.
Controlled behavior: p1.l0 -> p1.l0 and (p1.x = 0 and p2.y = 0).

Round 1: finished, need another round.

Round 2: started.

Round 2: computing backward controlled-behavior predicate.
Backward controlled-behavior: p1.l0 [marker predicate]
Backward controlled-behavior: p1.l0 -> p1.l0 and (p1.x = 0 and p2.y = 0) [restricted to current/previous controlled-behavior predicate: p1.l0 and (p1.x = 0 and p2.y = 0)]
Backward reachability: iteration 1.
Backward controlled-behavior: p1.l0 and (p1.x = 0 and p2.y = 0) [fixed point].

Round 2: computing backward uncontrolled bad-state predicate.
Backward uncontrolled bad-state: p1.l1 or (p1.x != 0 or p2.y != 0) [current/previous controlled behavior predicate]
Backward reachability: iteration 1.

Round 2: finished, controlled behavior is stable.

Computing controlled system guards.

Edge (event: a) (guard: p1.l0 and p2.y = 4 or (p1.l0 and p2.y = 5 or p1.l0 and p2.y = 3)) (assignments: p1.x := p1.x + 1, p1 := p1.l1, p2.y := 3): guard: p1.l0 and p2.y = 4 or (p1.l0 and p2.y = 5 or p1.l0 and p2.y = 3) -> false.
Edge (event: b) (guard: p1.l0 and p1.x = 5 or p1.l0 and p1.x = 3): guard: p1.l0 and p1.x = 5 or p1.l0 and p1.x = 3 -> false.

Final synthesis result:
  State: (controlled-behavior: p1.l0 and (p1.x = 0 and p2.y = 0))
    Edge: (event: a) (guard: p1.l0 and p2.y = 4 or (p1.l0 and p2.y = 5 or p1.l0 and p2.y = 3) -> false) (assignments: p1.x := p1.x + 1, p1 := p1.l1, p2.y := 3)
    Edge: (event: b) (guard: p1.l0 and p1.x = 5 or p1.l0 and p1.x = 3 -> false)

Controlled system:                     exactly 1 state.

Initial (synthesis result):            p1.l0 and (p1.x = 0 and p2.y = 0)
Initial (uncontrolled system):         p1.l0 and (p1.x = 0 and p2.y = 0)
Initial (controlled system):           p1.l0 and (p1.x = 0 and p2.y = 0)
Initial (removed by supervisor):       false
Initial (added by supervisor):         true

Constructing output CIF specification.
Writing output CIF file "datasynth/guards_upds.ctrlsys.real.cif".
