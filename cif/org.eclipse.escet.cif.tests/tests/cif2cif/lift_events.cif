//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2022 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

event x_y_z;
type t_e = bool;

automaton x_y:
  event z, a;
  uncontrollable u;
  disc bool v;
  location l:
    initial;
end

automaton x:
  event y_z, b;
  controllable c;
  location l:
    initial;
end

automaton t:
  disc t_e v;
  event e;
  location l:
    initial;
end

invariant {x_y_z, x_y.z, x_y.a, x_y.u} needs true;
invariant x_y.v;
invariant {x.y_z, x.b, x.c, t.e} needs true;
invariant t.v;
