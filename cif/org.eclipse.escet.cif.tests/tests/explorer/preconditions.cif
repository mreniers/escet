//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2022 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

plant pp:
  disc dist real d = constant(1.0);     // Distribution stdlib function.
  alg real x = (sample d)[0];           // Sampling.

  disc int d1 in any;                   // Multiple potential initial values.
  disc list string d2 in any;           // Multiple potential initial values.
  initial d1 = size(d2);                // Silence unused warnings.

  location:
    initial;
end

func int f(): "java:some.Class.Method"; // External user-defined function.

input bool i;                           // Input variable.

// Not yet included in this test as default state explorer allows them:
plant b:
  location a:
    initial;
    edge when true;                     // Implicit tau.

  location b:
    initial;
    edge tau;                           // Explicit tau.
end

            invariant false;            // Regular invariant.
plant       invariant false;
requirement invariant false;            // Requirement invariant.
supervisor  invariant false;            // Supervisor invariant.

automaton aa:
  location:
    initial;

                invariant false;        // Regular invariant.
    plant       invariant false;
    requirement invariant false;        // Requirement invariant.
    supervisor  invariant false;        // Supervisor invariant.
end

automaton a:                            // Regular automaton.
  location:
    initial;
end

plant p:
  location:
    initial;
end

requirement r:                          // Requirement automaton.
  location:
    initial;
end

supervisor s:                           // Supervisor automaton.
  location:
    initial;
end
