<?xml version="1.0" encoding="UTF-8"?>
<!--
  Copyright (c) 2010, 2022 Contributors to the Eclipse Foundation

  See the NOTICE file(s) distributed with this work for additional
  information regarding copyright ownership.

  This program and the accompanying materials are made available under the terms
  of the MIT License which is available at https://opensource.org/licenses/MIT

  SPDX-License-Identifier: MIT
-->
<project xmlns="http://maven.apache.org/POM/4.0.0" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance"
    xsi:schemaLocation="http://maven.apache.org/POM/4.0.0 http://maven.apache.org/xsd/maven-4.0.0.xsd">
    <modelVersion>4.0.0</modelVersion>
    <parent>
        <groupId>org.eclipse.escet</groupId>
        <artifactId>org.eclipse.escet.root</artifactId>
        <version>0.5.0-SNAPSHOT</version>
        <relativePath>../../</relativePath>
    </parent>

    <artifactId>org.eclipse.escet.releng.project.documentation</artifactId>
    <packaging>eclipse-plugin</packaging>

    <build>
        <plugins>
            <!-- Generate documentation from AsciiDoc sources. -->
            <plugin>
                <groupId>org.asciidoctor</groupId>
                <artifactId>asciidoctor-maven-plugin</artifactId>
                <configuration>
                    <sourceDirectory>asciidoc</sourceDirectory>
                    <sourceDocumentName>documentation.asciidoc</sourceDocumentName>
                    <attributes>
                        <imagesdir />
                    </attributes>
                </configuration>
                <executions>
                    <execution>
                        <id>generate-eclipse-help</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>process-asciidoc</goal>
                        </goals>
                        <configuration>
                            <outputFile>
                                ${project.build.directory}/eclipse-help-intermediate/documentation.html
                            </outputFile>
                            <backend>html5</backend>
                            <sourceHighlighter>coderay</sourceHighlighter>
                            <logHandler>
                                <failIf>
                                    <severity>DEBUG</severity>
                                </failIf>
                            </logHandler>
                            <attributes>
                                <attribute-missing>warn</attribute-missing>
                                <linkcss>true</linkcss>
                                <imgsdir>images</imgsdir>
                                <icons>font</icons>
                                <sectanchors>false</sectanchors>
                                <html-output>true</html-output>
                                <eclipse-help-output>true</eclipse-help-output>
                            </attributes>
                        </configuration>
                    </execution>
                    <execution>
                        <id>generate-pdf</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>process-asciidoc</goal>
                        </goals>
                        <configuration>
                            <outputFile>
                                ${project.build.directory}/website-intermediate/eclipse-escet-project-incubation-manual.pdf
                            </outputFile>
                            <backend>pdf</backend>
                            <sourceHighlighter>coderay</sourceHighlighter>
                            <logHandler>
                                <failIf>
                                    <severity>DEBUG</severity>
                                </failIf>
                            </logHandler>
                            <attributes>
                                <attribute-missing>warn</attribute-missing>
                                <imgsdir>${project.basedir}/images</imgsdir>
                                <doctype>book</doctype>
                                <icons>font</icons>
                                <pagenums />
                                <sectnums />
                                <toc />
                                <toclevels>2</toclevels>
                                <pdf-output>true</pdf-output>
                            </attributes>
                        </configuration>
                    </execution>
                    <execution>
                        <id>generate-website</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>process-asciidoc</goal>
                        </goals>
                        <configuration>
                            <outputFile>${project.build.directory}/website-intermediate/documentation.html</outputFile>
                            <backend>html5</backend>
                            <sourceHighlighter>coderay</sourceHighlighter>
                            <logHandler>
                                <failIf>
                                    <severity>DEBUG</severity>
                                </failIf>
                            </logHandler>
                            <attributes>
                                <attribute-missing>warn</attribute-missing>
                                <imgsdir>.</imgsdir>
                                <doctype>book</doctype>
                                <icons>font</icons>
                                <toc>left</toc>
                                <toclevels>2</toclevels>
                                <sectanchors>true</sectanchors>
                                <html-output>true</html-output>
                                <website-output>true</website-output>
                            </attributes>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- Split single-page HTML to multi-page HTML. -->
            <plugin>
                <groupId>org.codehaus.mojo</groupId>
                <artifactId>exec-maven-plugin</artifactId>
                <version>3.0.0</version>
                <configuration>
                    <mainClass>
                        org.eclipse.escet.common.asciidoc.html.multipage.AsciiDocMultiPageHtmlSplitter
                    </mainClass>
                    <includePluginDependencies>true</includePluginDependencies>
                    <includeProjectDependencies>true</includeProjectDependencies>
                </configuration>
                <executions>
                    <execution>
                        <id>eclipse-help-multi-html-split</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>java</goal>
                        </goals>
                        <configuration>
                            <arguments>
                                <argument>${project.basedir}/asciidoc</argument>
                                <argument>${project.build.directory}/eclipse-help-intermediate/documentation.html</argument>
                                <argument>${project.build.directory}/eclipse-help</argument>
                                <argument>--eclipse-help</argument>
                            </arguments>
                        </configuration>
                    </execution>
                    <execution>
                        <id>website-multi-html-split</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>java</goal>
                        </goals>
                        <configuration>
                            <arguments>
                                <argument>${project.basedir}/asciidoc</argument>
                                <argument>${project.build.directory}/website-intermediate/documentation.html</argument>
                                <argument>${project.build.directory}/website</argument>
                                <argument>--website</argument>
                                <argument>Eclipse ESCET™ website</argument>
                                <argument>.</argument>
                            </arguments>
                        </configuration>
                    </execution>
                </executions>
                <dependencies>
                    <dependency>
                        <groupId>org.eclipse.escet</groupId>
                        <artifactId>org.eclipse.escet.common.asciidoc</artifactId>
                        <version>${project.version}</version>
                        <type>eclipse-plugin</type>
                    </dependency>
                    <dependency>
                        <groupId>org.eclipse.escet</groupId>
                        <artifactId>org.eclipse.escet.common.java</artifactId>
                        <version>${project.version}</version>
                        <type>eclipse-plugin</type>
                    </dependency>
                    <dependency>
                        <groupId>org.eclipse.escet</groupId>
                        <artifactId>org.eclipse.escet.common.app.framework</artifactId>
                        <version>${project.version}</version>
                        <type>eclipse-plugin</type>
                    </dependency>
                </dependencies>
            </plugin>

            <!-- Copy non-HTML files from intermediate output folders to final output folders. -->
            <plugin>
                <groupId>org.apache.maven.plugins</groupId>
                <artifactId>maven-antrun-plugin</artifactId>
                <version>3.0.0</version>
                <executions>
                    <execution>
                        <id>copy-intermediate-to-final</id>
                        <phase>generate-resources</phase>
                        <goals>
                            <goal>run</goal>
                        </goals>
                        <configuration>
                            <target>
                                <copy todir="${project.build.directory}/eclipse-help" verbose="true">
                                    <fileset dir="${project.build.directory}/eclipse-help-intermediate">
                                        <exclude name="**/*.html" />
                                    </fileset>
                                </copy>
                                <copy todir="${project.build.directory}/website" verbose="true">
                                    <fileset dir="${project.build.directory}/website-intermediate">
                                        <exclude name="**/*.html" />
                                    </fileset>
                                </copy>
                            </target>
                        </configuration>
                    </execution>
                </executions>
            </plugin>

            <!-- Package the Eclipse help contents in the plugin. -->
            <plugin>
                <groupId>org.eclipse.tycho</groupId>
                <artifactId>tycho-packaging-plugin</artifactId>
                <configuration>
                    <additionalFileSets>
                        <fileSet>
                            <directory>${project.build.directory}/eclipse-help/</directory>
                            <includes>
                                <include>**/*</include>
                            </includes>
                        </fileSet>
                    </additionalFileSets>
                </configuration>
            </plugin>

            <!-- Assemble website ZIP file. -->
            <plugin>
                <artifactId>maven-assembly-plugin</artifactId>
                <executions>
                    <execution>
                        <id>assemble-website</id>
                        <phase>package</phase>
                        <goals>
                            <goal>single</goal>
                        </goals>
                        <configuration>
                            <finalName>escet-project-website-${escet.version.enduser}</finalName>
                            <appendAssemblyId>false</appendAssemblyId>
                            <descriptors>
                                <descriptor>${basedir}/assembly.xml</descriptor>
                            </descriptors>
                        </configuration>
                    </execution>
                </executions>
            </plugin>
        </plugins>
    </build>
</project>
