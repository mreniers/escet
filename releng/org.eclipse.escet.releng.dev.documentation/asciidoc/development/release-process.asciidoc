//////////////////////////////////////////////////////////////////////////////
// Copyright (c) 2010, 2022 Contributors to the Eclipse Foundation
//
// See the NOTICE file(s) distributed with this work for additional
// information regarding copyright ownership.
//
// This program and the accompanying materials are made available
// under the terms of the MIT License which is available at
// https://opensource.org/licenses/MIT
//
// SPDX-License-Identifier: MIT
//////////////////////////////////////////////////////////////////////////////

include::_part_attributes.asciidoc[]

[[development-release-process-chapter-index]]
== Release process

This page explains step by step the various processes involved from preparing for a release to actually releasing it.
The following topics are covered:

* <<development-release-process-release-policy>>
* <<development-release-process-plan-next-version>>
* <<development-release-process-prepare-git-repo-for-next-version>>
* <<development-release-process-working-on-a-release>>
* <<development-release-process-prepare-release>>
* <<development-release-process-perform-release>>

[[development-release-process-release-policy]]
=== Release policy

The Eclipse ESCET project uses a time-based release policy.
We generally release every three months, at the end of each quarter, typically on its last working day.

We may however deviate from this.
For instance, we may release earlier at the end of the year, well before the Christmas period.

[[development-release-process-plan-next-version]]
=== Planning for a next version

For a new version (not milestone or release candidate), follow these steps.
Perform them well in advance of starting work on the version, i.e. while still working on the previous version:

* Create a new release record:

** Go to https://projects.eclipse.org/projects/technology.escet.

** Log in with your Eclipse Foundation account using the link at the top-right of the page.

** Click the _Create a new release_ link in the bar at the right.

** Set the planned release date and give the release a name, e.g. `0.1`, `0.1.1` or `1.0`.

** Click _Create and edit_.

** For _Description_, click the _Source_ button in the toolbar of the editor.
Then enter `<p>See <a href="https://gitlab.eclipse.org/eclipse/escet/escet/-/milestones/NNN">Eclipse ESCET GitLab vN.N issues</a> for more information.</p>`.
Replace `NNN` by the actual GitLab milestone number to ensure a correct URL.
Replace `vN.N` by the version, e.g. `v0.1`, `v0.1.1` or `v1.0`.

** Change the _Release type_ if applicable.

** For _IP Due Diligence Type_ select _Type A - License Compatibility Certification_.

* Create a GitLab milestone:

** Go to https://gitlab.eclipse.org/eclipse/escet/escet/-/milestones.

** Sign in with your Eclipse Foundation account using the link at the top-right of the page.

** Click _New milestone_.

** For _Title_ enter the version, e.g. `v0.1`, `v0.1.1` or `v1.0`.

** For _Description_ enter `See also https://projects.eclipse.org/projects/technology.escet/releases/N.N.`.
Replace `N.N` by the actual release record version, `0.1`, `0.1.1` or `1.0`.

** For _Start date_ select the first day after the _Due date_ of the previous version.

** For _End date_ select the same date as the planned release date of the release record.

** Click _Create milestone_.

[[development-release-process-prepare-git-repo-for-next-version]]
=== Preparing Git repository for a next version

To prepare the Git repository for the next version (not a milestone or release candidate), follow these steps:

* Run in the root of the Git repository the command `./mvn_escet.sh org.eclipse.tycho:tycho-versions-plugin:set-version -DnewVersion=<new-version>`, where `<new-version>` is replaced by the new version, e.g. `0.2.0.qualifier`.
This replaces most version numbers automatically.
Check all changes to ensure no versions are updated that should not be updated.

* Verify that the `feature` `url` and `version` of all `org.eclipse.escet.*` features in `org.eclipse.escet.product/category.xml` are properly updated.
The old version should no longer be present.
This should be automatic.

* Verify that the `product` `version` in `org.eclipse.escet.product/escet.product` is properly updated.
The old version should no longer be present.
This should be automatic.

* Verify that the `feature` `version` of the product feature in `org.eclipse.escet.product/escet.product` is properly updated.
The old version should no longer be present.
This should be automatic.

* Verify that the `Bundle-Version` of all `MANIFEST.MF` files in `org.eclipse.escet.*` projects are properly updated.
The old version should no longer be present.
This should be automatic.

* Manually replace in all `MANIFEST.MF` files the regex `+org\.eclipse\.escet\.([a-z0-9\.]+);bundle-version="<old-version>"+` by `+org.eclipse.escet.\1;bundle-version="<new-version>"+`, where `<old-version>` and `<new-version>` are replaced by actual versions, e.g. `0.1.0`.
Check for unintended changes.
Search for the old version in all manifests to ensure none remain.

* In all documentation sets, add a new section to the release notes for the new version, before the existing versions:
+
[source]
----
=== Version 0.2

TBD
----

* Verify that the `<version>` of a POM and its parent in each `pom.xml` are properly updated.
The old version should no longer be present.
This should mostly be automatic.
Manually adapt it where necessary.

** Manually adapt the version in `org.eclipse.escet.releng.configuration/pom.xml`.

** Manually adapt the parent version in `org.eclipse.escet.root/pom.xml`.
Also remove the version for the pom itself, to prevent a warning that the version of the pom and its parent are identical.

* Verify that the `version` of all `feature.xml` files in `org.eclipse.escet.*` projects are properly updated.
The old version should no longer be present.
This should be automatic.

* Verify that the `license-feature-version` of all `feature.xml` files in `org.eclipse.escet.*` projects are properly updated.
The old version should no longer be present.
This should be automatic.

* Manually replace in all `feature.xml` files the regex `+<import feature="([^"]+)" version="<old-version>"/>+` by `+<import feature="\1" version="<new-version>"/>+`, where `<old-version>` and `<new-version>` are replaced by actual versions, e.g. `0.1.0.qualifier`.
Check for unintended changes.

* Manually search for the old version in all `features.xml` files to ensure none remain.

* Search the entire Git repository (all projects) for the old version number and update anything that still requires updating.

* Test that the <<development-building-and-testing-chapter-index,build works>>.

[[development-release-process-working-on-a-release]]
=== Working on a release

The work for the next release (milestone, release candidate, or final release) is done in the `develop` branch.
See the <<development-development-process-chapter-index,development process>> for more information.

Note that for each version at least one milestone release and at least one release candidate are required before a final release.

[[development-release-process-prepare-release]]
=== Preparing a release

Once the work on `develop` is done for a release (milestone, release candidate, or final release), follow these steps to prepare for the release:

* Double check that the release notes of all documentation sets have been updated for all end-user visible changes.

* For a final release add the release date to the release notes.

* For a final release ensure the `TBD` indication is removed in the release notes of all documentation sets.

* For a final release ensure that all IP is accounted for and all CQs have been approved by the Eclipse Foundation IP team.

* For a final release ensure that a release review has been successfully completed no more than one year ago.
For a release review follow these steps:

** Prepare the release review material in the release record.
See e.g. https://projects.eclipse.org/projects/technology.escet/reviews/0.1-release-review.

** Submit the IP log.
See e.g. https://dev.eclipse.org/ipzilla/show_bug.cgi?id=23179.

** Request PMC approval.
See e.g. https://www.eclipse.org/lists/technology-pmc/msg12371.html.

** Ask the EMO to schedule the release review and make it public.
Consider that release reviews are to be scheduled to happen on the 1st or 3rd Wednesday of a month.
See e.g. https://www.eclipse.org/lists/escet-dev/msg00091.html (mail also sent to \emo@eclipse.org).

** Ensure the release review is successful before performing the release.

[[development-release-process-perform-release]]
=== Performing a release

To perform a release (milestone, release candidate, or final release), i.e. actually release it, follow these steps:

* Create a GitLab merge request from `develop` to `master`, and name it after the release.
Since `master` is a protected branch for the Eclipse ESCET GitLab, a GitLab merge request is the only way to update it.

* Ensure the merge request is successfully <<development-building-and-testing-chapter-index,built on Jenkins>>.
Then merge the merge request.

* Check that the build on `master` succeeds in Jenkins.

* Add a tag on the commit in `master` that is to be released.
Only version tags with a specific syntax will be picked up by Jenkins to be released.
For instance, use `v0.1`, `v0.1.1`, `v2.0`, etc for releases, `v0.1-M1` for a milestone build, or `v0.1-RC1` for a release candidate.
+
Add the tag via GitLab, at https://gitlab.eclipse.org/eclipse/escet/escet/-/tags/new.
Use the _Tag name_ also as _Message_ and as _Release notes_.
Make sure to select `master` as branch from which to create the tag.

* Go to https://gitlab.eclipse.org/eclipse/escet/escet/-/releases and edit the new GitLab release by clicking the _Edit this release_ icon at the top-right of the release.
Add the GitLab milestone to the GitLab release, by selecting it under _Milestones_.
Save the changes.

* Go to Jenkins, at https://ci.eclipse.org/escet/job/ESCET%20build/.
Log in to Jenkins by clicking on the link at the top-right of the page.
Select _Scan GitLab Project Now_ to ensure Jenkins picks up the new tag.

* Go to https://ci.eclipse.org/escet/job/ESCET%20build/view/tags/ to see the new tag on Jenkins.
Manually trigger a build for the tag, by clicking the _Schedule a build for ..._ icon in the row for the tag.
Jenkins will then automatically build and release a new version from that tag.

* All releases are available at https://download.eclipse.org/escet/.
For a version `v0.1`, the downloads will be located at `\https://download.eclipse.org/escet/v0.1`.
+
Note that according to the Eclipse Foundation Wiki page link:https://wiki.eclipse.org/IT_Infrastructure_Doc[IT Infrastructure Doc], "Once your files are on the `download.eclipse.org` server, they are immediately available to the general public.
However, for release builds, we ask that you wait at least four hours for our mirror sites to fetch the new files before linking to them.
It typically takes a day or two for all the mirror sites to synchronize with us and get new files."
+
That same wiki page also notes that "Although you can link directly to `download.eclipse.org/yourfile.zip`, you can also use the _Find a Mirror_ script [...].
Using this script allows you to view download statistics and allows users to pick a nearby mirror site for their download."
It further indicates that "P2 repositories are not normally accessed via the mirror selection script."
The _Find a Mirror_ script also transparently handles files moved from `download.eclipse.org` to `archive.eclipse.org`.
End users should thus be referred to https://eclipse.org/escet/download.html instead of to `download.eclipse.org`, to ensure the mirror script is used for faster downloads and to ensure downloads count in the download statistics.

* Jenkins will automatically push the website for the new release to the website Git repository, in a directory for the specific release.
For a version `v0.1`, the website can be accessed at `\https://eclipse.org/escet/v0.1`.
It may take a few minutes for the Git repository to be synced to the webserver and for the website for the new version to become available.

* If the website for the new release is to be the standard visible website for the project (at `https://eclipse.org/escet`), it has to be manually replaced.
This is to ensure that a bugfix release for an older version doesn't override the standard visible website.
The following steps explain how to 'promote' a website for a specific version to become the standard visible website:

** Make sure you've uploaded your SSH public key to Eclipse Gerrit.
This is a one-time only step.
Go to https://git.eclipse.org/r/.
Sign in using your Eclipse Foundation committer account.
Use the gear icon at the top right to access your account settings.
Under _SSH Keys_ add your SSH public key.
Also make note of your _username_, _Full name_ and _Email_ address.

** Clone the Eclipes ESCET website Git repository using `git clone ssh://<username>@git.eclipse.org:29418/www.eclipse.org/escet.git`.
Make sure to replace `<username>` by your Eclipse Foundation committer account _username_.

** In the cloned repository, remove all files/folders in the root of the Git repository pertaining to the current standard visible website.
Be sure not to remove any of the directories with websites for specific releases.

** Copy the files/folders from the directory with the website for the release that you want to make the standard visible website, and put them in the root of the Git repository.

** Add all changes to be committed, e.g. by using `git add -A`.

** Make sure to use the _Full name_ and _Email_ address of your Eclipse Foundation committer account.
E.g. use `git config --local user.name "<full_name>"` and `git config --local user.email "<email>"`, replacing `<full_name>` and `<email>` by the appropriate information matching your Eclipse Foundation committer account.

** Commit the changes.
Use as commit message `Set standard visible website to release <version>.`, replacing `<version>` by the release version that will become the new standard visible website.
Make sure to sign off the commit to pass Eclipse Foundation automatic commit validation.
E.g. use the following to commit the changes:
`git commit -s -m "Set standard visible website to release v0.1."`.

** Push the changes to the Git server.
E.g. use `git push`.
If successful you should see the changes at the Git server's web view, at https://git.eclipse.org/c/www.eclipse.org/escet.git/.

** It may take a few minutes for the Git repository to be synced to the webserver, and for the new standard visible website to become available.
The standard visible website can be accessed at `https://eclipse.org/escet`.
Depending on browser cache settings and other factors, it may be necessary to force refresh your browser for it to pick up the changes on the server.

* Remove/archive old releases:

** For every release (not a milestone or release candidate), remove all milestones and release candidates of the previous version and older.
E.g. for `v0.2`, remove `v0.1-M1`, `v0.1-M2`, `v0.1-RC1`, etc.
Also archive all releases older than the current and previous release.
E.g. for `v0.3` archive `v0.1` and older, but keep `v0.2`.
+
Consider whether archiving or removing P2 update sites will lead to issues for users.
See for more information the Eclipse Foundation Wiki page link:https://wiki.eclipse.org/Equinox/p2/p2.mirrorsURL#Moving_a_repo_to_archive.eclipse.org[Moving a repo to archive.eclipse.org].

** Old websites can be removed in a similar way to the above instructions to change the default website.
Only remove the directory for the milestone or release candidate.

** Older downloads can be archived.
Go to https://download.eclipse.org/escet/.
Make sure you're logged in.
This should make check-boxes appear.
Select the folders to archive and click the _Archive_ button.
It may take a few minutes for archiving to complete.

** Archived downloads can be removed.
Go to https://archive.eclipse.org/escet/.
Make sure you're logged in.
This should make check-boxes appear.
Select the folders to delete and click the _Delete_ button.
It may take a few minutes for deleting to complete.
